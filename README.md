# API Notation Syntax for Atom

This is a grammar for api notation, a simple notation I've been using for
annotating module APIs in specs and other documents.

## Installing

### From Command Line

`apm install api-notation`

### From GUI

#### Open the Package Manager from Palette Manager

1. Press `⌘⇧P` on OSX or `⌃⇧P` on Windows/Linux to open the palette manager
2. Look for `Install Packages and Themes`

#### Open the Package Manager from Preferences

1. Go to `Atom -> Preferences` menu
2. Go to `Install`

#### Install it

1. Search for `api-notation` in the search bar
2. Click Install

## Usage

Open a `.api` file, or set it as your syntax.

## API Notation Reference

The most recent reference as of now is in this [gist][api-reference], so
check that to be sure. But here's a reproduction of the notation.

```
NameOfClass.WithPossibleNamespace
   + class property
   - instance property
  ~> listened events (socket)
  +> listened events (class/module)
  -> listened events (instance)
  <~ dispatched events (socket)
  <+ dispatched events(class/module)
  <- dispatched events (instance)
  :: class method
   # instance method

Other symbols
  => returns
->() callback return
[xx] optional
<xx> data type

Recommended order: class first, then sockets, then instance. Internally:
Properties, events, methods.

// Anything after two forward slashes is a comment
```

## How does it look like?

Like this.

![Like this.][api-example]

[api-reference]: https://gist.github.com/benbeltran/4694c46fcec131468e34
[api-example]: https://i.imgur.com/5WXuf4u.png
